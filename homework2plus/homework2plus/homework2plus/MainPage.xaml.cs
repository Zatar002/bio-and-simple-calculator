﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace homework2plus
{
    public partial class MainPage : ContentPage
    {
        int DIGITSMAX = 8;                             //maximum number of digits to be displayed 
        int counter = 1;                               //keep track of digit button presses/numbers being displayed
        bool isDigitPressed = false;
        bool isSymbolPressed = false;
        bool isEqualPressed = false;
        int var1 = 0;
        int var2 = 0;
        string symb = "";
        double solution = 0;

        public MainPage()
        {
            InitializeComponent();
        }

        private void Digit_Button_Clicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;             //assign the button that was pressed to button

            if (counter < DIGITSMAX && isEqualPressed == false)
            {
                if (isDigitPressed == false)                       //if this is the first digit press, overwrite the default 'zero' first 
                {
                    ansLabel.Text = button.Text;
                    isDigitPressed = true;
                }
                else
                {
                    ansLabel.Text += button.Text;       //add the text to the display label text
                    ++counter;                              //increase the counter
                }
            }

            //decided to remove this functionality
            /*else
            {
                ansLabel.Text += button.Text;
                ansLabel.Text = ansLabel.Text.Substring(1);       //if counter is more than 8, get rid of the leading digit
            }*/
        }

        private void Clear_Button_Clicked(object sender, EventArgs e)
        {
            if (ansLabel.Text != "0" || prevLabel.Text != "- - -")                           //only clear if a digit has been pressed or a symbol has been pressed
            {
                ansLabel.Text = "0";
                prevLabel.Text = "- - -";
                counter = 1;
                isDigitPressed = false;
                isSymbolPressed = false;
                isEqualPressed = false;
                var1 = 0;
                var2 = 0;
                symb = "";
                solution = 0;
            }
        }

        private void Zero_Button_Clicked(object sender, EventArgs e)
        {
            if (ansLabel.Text != "0")
                Digit_Button_Clicked(sender, e);        //if zero is clicked and the counter is zero, don't do anything
        }

        private void Symbol_Button_Clicked(object sender, EventArgs e)
        {
            if (isSymbolPressed == false)               //ignore any additional symbol calls
            {
                Button button = (Button)sender;
                var1 = Convert.ToInt32(ansLabel.Text);
                symb = button.Text;
                prevLabel.Text = ansLabel.Text + " " + button.Text + " ";
                isSymbolPressed = true;
                isDigitPressed = false;
                //ansLabel.Text = "0";
                counter = 1;
            }
        }

        private void Equal_Button_Clicked(object sender, EventArgs e)
        {

            if (isEqualPressed == false && isSymbolPressed == true)     //ignore any additional symbol calls, but a symbol must have been pressed previously
            {
                Button button = (Button)sender;
                prevLabel.Text += ansLabel.Text;
                prevLabel.Text += " = ";
                var2 = Convert.ToInt32(ansLabel.Text);

                if (var2 == 0 && symb == "/")
                {
                    ansLabel.Text = "/0 ERROR";
                    counter = -1;
                }
                if (symb == "/" && var2 != 0)
                {
                    solution = (double)var1 / (double)var2;
                    if (solution.ToString().Length <= DIGITSMAX)
                        ansLabel.Text = solution.ToString();
                    else
                        //ansLabel.Text = solution.ToString("E5");      //round off to 5 sig. figs.
                        ansLabel.Text = solution.ToString("0.0000000");  //using custom string to eliminate leading zeros in the exponent 'E' notation
                }
                else if(symb == "X")
                {
                    solution = (double)var1 * (double)var2;

                    /*no need to check range, largest value possible is 99999999 * 99999999. It can be displayed */

                    if (solution.ToString().Length <= DIGITSMAX)
                        ansLabel.Text = solution.ToString();
                    else
                        //ansLabel.Text = solution.ToString("E5");      //round off to 5 sig. figs.
                        ansLabel.Text = solution.ToString("0.00000E0");  //using custom string to eliminate leading zeros in the exponent 'E' notation
                }
                else if (symb == "-")
                {
                    solution = (double)var1 - (double)var2;
                    ansLabel.Text = solution.ToString();
                }
                else if (symb == "+")
                {
                    solution = (double)var1 + (double)var2;
                    if (solution.ToString().Length <= DIGITSMAX)
                        ansLabel.Text = solution.ToString();
                    else
                        //ansLabel.Text = solution.ToString("E5");      //round off to 5 sig. figs.
                        ansLabel.Text = solution.ToString("0.00000E0");  //using custom string to eliminate leading zeros in the exponent 'E' notation
                }
                isEqualPressed = true;
            }
        }
    }
}
