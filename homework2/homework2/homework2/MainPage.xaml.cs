﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace homework2
{
    public partial class MainPage : ContentPage
    {
        int DISPLAYMAX = 8;                             //maximum number of digits to be displayed 
        int counter = 0;                                //keep track of digit button presses/numbers being displayed

        public MainPage()
        {
            InitializeComponent();
        }

        private void Digit_Button_Clicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;             //assign the button that was pressed to button

            if (counter < DISPLAYMAX)
            {
                if (counter == 0)                       //if this is the first digit press, overwrite the default 'zero' first 
                    ansLabel.Text = button.Text;
                else
                    ansLabel.Text += button.Text;       //add the text to the display label text

                ++counter;                              //increase the counter
            }

            else
            {
                ansLabel.Text += button.Text;                
                ansLabel.Text = ansLabel.Text.Substring(1);       //if counter is more than 8, get rid of the leading digit
            }
        }

        private void Clear_Button_Clicked(object sender, EventArgs e)
        {
            if (counter != 0)                           //only clear if a digit has been pressed
            {
                ansLabel.Text = "0";
                counter = 0;
            }
        }

        private void Digit_Zero_Clicked(object sender, EventArgs e)
        {
            if (counter != 0)
                Digit_Button_Clicked(sender, e);        //if zero is clicked and the counter is zero, don't do anything
        }
    }
}
