﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace week2lab
{
    public partial class MainPage : ContentPage
    {
        int counter = 0;
        public MainPage()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            counter++;
            clickedCountLabel.Text = counter.ToString();
        }
        private void Button_Clicked_Ten(object sender, EventArgs e)
        {
            counter += 10;
            clickedCountLabel.Text = counter.ToString();
        }
    }
}
